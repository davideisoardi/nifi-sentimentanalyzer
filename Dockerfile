FROM maven:3.5.4-jdk-8 as assembly_nifi
WORKDIR /app
COPY --from=clone_nifi /app/nifi /app
RUN ls -l
RUN mvn -T 2.0C clean install -DskipTests

FROM alpine/git as clone_nifi-sentimentanalyzer
ARG NIFI_SENTIMENT_VERSION_TAG=v2.0-NiFi_1.12.1-STDF_NLP_4.0
ARG NIFI_SENTIMENT_VERSION=2.0
WORKDIR /app
RUN git clone --depth 1 --branch ${NIFI_SENTIMENT_VERSION_TAG} https://bitbucket.org/davideisoardi/nifi-sentimentanalyzer.git